const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const {bytecode, interface} = require('../compile.js');

let accounts;
let inbox;

beforeEach(async () => {

    // Get account

    accounts = await web3.eth.getAccounts();
    console.log(accounts);

    // Deployment
    inbox = await new web3.eth.Contract(JSON.parse(interface)).deploy({ data: bytecode, arguments: ['Hi!']})
        .send({ from: accounts[0], gas: '1000000'});
    
});

describe('tests', () => {
    it('deploy contract',() => {
     console.log(inbox);    
     assert.ok(inbox.options.address);
    });

    it('get message',async () =>{
        const message = await inbox.methods.message().call();
        assert.equal(message,'Hi!');
    });

    it('set message',async () => {
        await inbox.methods.setMessage('Hello').send({from: accounts[0]});
        const message = await inbox.methods.message().call();
        assert.equal(message,'Hello');
    })
});
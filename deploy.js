const hdWallet = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const {bytecode, interface} = require('./compile.js')

const provider = new hdWallet(
    'merge champion afraid motor identify govern field turkey depth paddle ordinary identify',
    'https://rinkeby.infura.io/v3/0ae5c8d91f05424aa662ee5e19c5a7ba'  
);

const web3 = new Web3(provider);

const deploy = async () => {
    const accounts = await web3.eth.getAccounts();


    const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({data: bytecode , arguments: ['I love you CA']})
    .send({gas: '1000000', from: accounts[0]});

    console.log(accounts[0] + ' is used for contract deployed to ' + result.options.address);
}
deploy();